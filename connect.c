#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "efm32.h"
#include "efm32_chip.h"
#include "efm32_gpio.h"
#include "efm32_usart.h"
#include "efm32_cmu.h"
#include "connect.h"
#include "config.h"

#define	LED_PORT 0

#define MAGIC_WORD 0x534c
#define MAGIC_RESPONSE 0x4950
#define TEST_WORD 0x5445
#define TEST_RESPONSE 0x5354

uint64_t bitBuffer = 0;
PacketHandlerType packetHandler = NULL;
ConnectionState connectionState = DISCONNECTED;

void LEDOn(LEDPin pin) {
  GPIO->P[LED_PORT].DOUT &= ~(1 << pin);
}

void LEDOff(LEDPin pin) {
	GPIO->P[LED_PORT].DOUT |= (1 << pin);
}

void LEDToggle(LEDPin pin) {
	GPIO->P[LED_PORT].DOUT ^= (1 << pin);
}

void UART1_RX_IRQHandler(void) {
	uint8_t word = USART_Rx(UART1);
	int end = !(0x1 & word);
	bitBuffer = (bitBuffer << 7) | (word >> 1);
	if (end) {
		handleBits(bitBuffer);
		bitBuffer = 0;
	}
	USART_IntClear(UART1, UART_IF_RXDATAV);
}

void sendBits(uint64_t bits, uint8_t length) {
	int msd = length - 1;
	// for (int i = 63; i >= 0; i--) {
	// 		if (bits >> i & 0x1) {
	// 			msd = i;
	// 			break;
	// 		}
	// }
	
	int packet_count = (msd + 1) / 7;
	if ((msd + 1) % 7 > 0) packet_count++;
	
	for(int i = 0; i < packet_count; i++)
	{
		uint8_t packet = (bits >> (i * 7)) << 1;
		if (i < (packet_count - 1)) {
			packet |= 0x01;
		}
		else {
			packet &= 0xfe;
		}
		USART_Tx(UART1, packet);
	}
}

void handleBits(uint64_t bits) {
  if (bits == MAGIC_WORD) {
    sendBits(MAGIC_RESPONSE, 32);
    connectionState = CONNECTED;
    LEDOn(LED_BLUE);
  }
	else {
	  packetHandler(bits);
	}
}

void waitForConnect(PacketHandlerType handler) {
	packetHandler = handler;
}

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "efm32.h"
#include "efm32_chip.h"
#include "efm32_emu.h"
#include "efm32_gpio.h"
#include "efm32_i2c.h"
#include "efm32_usart.h"
#include "efm32_rtc.h"
#include "efm32_cmu.h"
#include "efm32_adc.h"
#include "efm32_timer.h"
#include "efm32_int.h"
#include "remote.h"
#include "efm32_timer.h"
#include "efm32_cmu.h"
#include "efm32_emu.h"

#define TOP 100000
bool setupPerformed = false;

void usePlug(PLUGS plug, BUTTONS button){

	if(plug == PLUG_A){
		if(button == BUTTON_ON){
			GPIO_PinModeSet(gpioPortE,4,gpioModePushPull,1);
			GPIO_PinModeSet(gpioPortE,6,gpioModePushPull,1);
		}
		if(button == BUTTON_OFF){
			GPIO_PinModeSet(gpioPortE,4,gpioModePushPull,1);
			GPIO_PinModeSet(gpioPortE,9,gpioModePushPull,1);
		}
	}
	if(plug == PLUG_B){
		if(button == BUTTON_ON){
			GPIO_PinModeSet(gpioPortE,5,gpioModePushPull,1);
			GPIO_PinModeSet(gpioPortE,6,gpioModePushPull,1);
		}
		if(button == BUTTON_OFF){
			GPIO_PinModeSet(gpioPortE,5,gpioModePushPull,1);
			GPIO_PinModeSet(gpioPortE,9,gpioModePushPull,1);
		}
	}
	if(plug == PLUG_C){
		if(button == BUTTON_ON){
			GPIO_PinModeSet(gpioPortE,7,gpioModePushPull,1);
			GPIO_PinModeSet(gpioPortE,6,gpioModePushPull,1);
		}
		if(button == BUTTON_OFF){
			GPIO_PinModeSet(gpioPortE,7,gpioModePushPull,1);
			GPIO_PinModeSet(gpioPortE,9,gpioModePushPull,1);
		}
	}
	
	/*for (int i = 0; i < 1000000; i++) {
		
				}
	stopTransmit();*/
		
	if (setupPerformed == false){
			setupRemote();
		}
	
}

/*void stopTransmit() {
	if (setupPerformed == true){
		GPIO_PinModeSet(gpioPortE, 4, gpioModePushPull, 0);
		GPIO_PinModeSet(gpioPortE, 5, gpioModePushPull, 0);
		GPIO_PinModeSet(gpioPortE, 6, gpioModePushPull, 0);
		GPIO_PinModeSet(gpioPortE, 7, gpioModePushPull, 0);
		GPIO_PinModeSet(gpioPortE, 9, gpioModePushPull, 0);	
	}
}*/

void TIMER0_IRQHandler(void)
{ 
  /* Clear flag for TIMER0 overflow interrupt */
  TIMER_IntClear(TIMER0, TIMER_IF_OF);
  
  /* Turn off the remote */
  GPIO_PinModeSet(gpioPortE, 4, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortE, 5, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortE, 6, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortE, 7, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortE, 9, gpioModePushPull, 0);	
  
}

void setupRemote(){
	
    /* Enable clock for TIMER0 module */
    CMU_ClockEnable(cmuClock_TIMER0, true);
	
    /* Select TIMER0 parameters */  
    TIMER_Init_TypeDef timerInit =
    {
      .enable     = true, 
      .debugRun   = true, 
      .prescale   = timerPrescale1024, 
      .clkSel     = timerClkSelHFPerClk, 
      .fallAction = timerInputActionNone, 
      .riseAction = timerInputActionNone, 
      .mode       = timerModeUp, 
      .dmaClrAct  = false,
      .quadModeX4 = false, 
      .oneShot    = false, 
      .sync       = false, 
    };
	
    /* Enable overflow interrupt */
    TIMER_IntEnable(TIMER0, TIMER_IF_OF);
  
    /* Enable TIMER0 interrupt vector in NVIC */
    NVIC_EnableIRQ(TIMER0_IRQn);
  
    /* Set TIMER Top value */
    TIMER_TopSet(TIMER0, TOP);
  
    /* Configure TIMER */
    TIMER_Init(TIMER0, &timerInit);
	
	setupPerformed = true;
	
 
}
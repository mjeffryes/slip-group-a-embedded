#define MAX_ADDR 0xb7
#define INT 0x00
#define INTS 0x01
#define LUX_HI 0x03
#define LUX_LO 0x04
#define cont 0x02
#define UTL 0x05
#define LTL 0x06
#define timer 0x07

int MAX_RegisterGet(I2C_TypeDef *i2c,
                         uint8_t addr,
                         uint8_t reg,
                         uint8_t *buf,
						 uint8_t len);
void MAXRegWrite(uint8_t reg, uint8_t val);
void MAXRegReadN(uint8_t reg, uint8_t len, uint8_t *buf);

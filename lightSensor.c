#include <stddef.h>
#include "efm32.h"
#include "efm32_i2c.h"
#include "config.h"
#include "MMA845XQ.h"
#include "lightSensor.h"

int MAX_RegisterGet(I2C_TypeDef *i2c,
                         uint8_t addr,
                         uint8_t reg,
                         uint8_t *buf,
						 uint8_t len)
{
  I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t regid[1];
  //uint8_t data[1];

  seq.addr = MAX_ADDR;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select register to be read */
  regid[0] = reg;
  seq.buf[0].data = regid;
  seq.buf[0].len = 1;
  seq.buf[1].data = buf; 
  seq.buf[1].len = len;
 
  ret = I2CDRV_Transfer(&seq);
  if (ret != i2cTransferDone)
  {
    return((int)ret);
  }

  return(0);
}

void MAXRegWrite(uint8_t reg, uint8_t val) {
   I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t regid[1];
  //uint8_t data[1];

  seq.addr = MAX_ADDR;
  seq.flags = I2C_FLAG_WRITE_WRITE;
  /* Select register to be read */
  regid[0] = reg;
  seq.buf[0].data = regid;
  seq.buf[0].len = 1;
  seq.buf[1].data = &val;
  seq.buf[1].len = 1;
 
  ret = I2CDRV_Transfer(&seq);
}

uint8_t MAXRegRead(uint8_t reg) {
  
  I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t regid[1];
  uint8_t data[1];

  seq.addr = MAX_ADDR;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select register to be read */
  regid[0] = reg;
  seq.buf[0].data = regid;
  seq.buf[0].len = 1;
  seq.buf[1].data = data;
  seq.buf[1].len = 1;
 
  ret = I2CDRV_Transfer(&seq);
  if (ret != i2cTransferDone)
  {
    return 0;
  }

  return data[0];
}

void MAXRegReadN(uint8_t reg, uint8_t len, uint8_t *buf)
{
	MAX_RegisterGet(I2C0, MAX_ADDR, reg, buf, len);
}

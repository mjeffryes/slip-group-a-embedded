#include <stdint.h>
#include <stdbool.h>
#include "config.h"
#include "connect.h"
#include "remote.h"
#include "lightSensor.h"
#include "MMA845XQ.h"
#include "efm32_chip.h"

bool light_on = false;

void handlePacket(uint64_t packet) {
	if (light_on) {
		usePlug(PLUG_A, BUTTON_OFF);
		usePlug(PLUG_B, BUTTON_ON);
		
		light_on = false;
	}
	else {
		usePlug(PLUG_A, BUTTON_ON);
		usePlug(PLUG_B, BUTTON_OFF);
		
		light_on = true;
	}
}

void main() {
  CHIP_Init();
  SystemCoreClockUpdate();
  IO_Init();
	uart_init(UART1);
	
	waitForConnect(&handlePacket);
	
	usePlug(PLUG_A, BUTTON_OFF);
	usePlug(PLUG_B, BUTTON_OFF);
	
	int delay = 0;
	while (true) {
		uint8_t buf[192 *2];
		if (delay > 500000) {
			MAXRegReadN(LUX_HI, 1, buf);
			sendBits((uint64_t) buf[0]);
			delay = 0;
		}
		delay++;
	}
}


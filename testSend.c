#include <stdint.h>
#include <stdbool.h>
#include "config.h"
#include "connect.h"
#include "remote.h"
#include "efm32_usart.h"
#include "efm32_chip.h"


bool light_on = false;

void handlePacket(uint64_t packet) {
	if (light_on) {
		usePlug(PLUG_A, BUTTON_OFF);
		usePlug(PLUG_B, BUTTON_ON);
		
		light_on = false;
	}
	else {
		usePlug(PLUG_A, BUTTON_ON);
		usePlug(PLUG_B, BUTTON_OFF);
		
		light_on = true;
	}
}

void main()
{
  CHIP_Init();
  SystemCoreClockUpdate();
  IO_Init();
	uart_init(UART1);
	
	waitForConnect(handlePacket);
	usePlug(PLUG_A, BUTTON_OFF);
	usePlug(PLUG_B, BUTTON_OFF);
	while (true) {}
}

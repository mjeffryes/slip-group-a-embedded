#include <stdint.h>
#include <stdbool.h>
#include "efm32_timer.h"
#include "efm32_letimer.h"
#include "efm32_gpio.h"
#include "config.h"
#include "connect.h"
#include "lightSensor.h"
#include "MMA845XQ.h"
#include "efm32_chip.h"
#include "router.h"
#include "efm32_cmu.h"
#include "tempSensor.h"
#include "radio.h"



#define LIGHT_SENSOR 4
#define IR_SENSOR 5
#define TEMP_SENSOR 6
#define TOP 1000000000
#define SELF 0



/* Maximum value for compare registers */
#define COMPMAX 1000000000

/* COMP1 is changed throughout the code to vary the PWM duty cycle 
   but starts with the maximum value (100% duty cycle) */
uint16_t comp1 = COMPMAX;

// void sense() {
// 	int delay = 0;
// 	while (true) {
// 		uint8_t buf[192 *2];
// 		if (delay > 500000) {
// 			MAXRegReadN(LUX_HI, 1, buf);
// 			sendPacket(LIGHT_SENSOR, (uint16_t)buf[0]);
// 			delay = 0;
// 		}
// 		delay++;
// 	}
// }

// void TIMER1_IRQHandler()
// { 
//   /* Clear flag for TIMER0 overflow interrupt */
//   TIMER_IntClear(TIMER1, TIMER_IF_OF);
//   
//   /* Turn off the remote */
//   	uint8_t buf[192 *2];
// 	MAXRegReadN(LUX_HI, 1, buf);
// 	sendPacket(LIGHT_SENSOR, (uint16_t)buf[0]);
//   
// }

void setupSensor(){
	    // 
	    // /* Enable clock for TIMER0 module */
	    // CMU_ClockEnable(cmuClock_TIMER1, true);
	    // 	
	    // /* Select TIMER0 parameters */  
	    // TIMER_Init_TypeDef timerInit =
	    //     {
	    //       .enable     = true, 
	    //       .debugRun   = true, 
	    //       .prescale   = timerPrescale1024, 
	    //       .clkSel     = timerClkSelHFPerClk, 
	    //       .fallAction = timerInputActionNone, 
	    //       .riseAction = timerInputActionNone, 
	    //       .mode       = timerModeUp, 
	    //       .dmaClrAct  = false,
	    //       .quadModeX4 = false, 
	    //       .oneShot    = false, 
	    //       .sync       = false, 
	    //     };
	    // 	
	    // /* Enable overflow interrupt */
	    // TIMER_IntEnable(TIMER1, TIMER_IF_OF);
	    //   
	    // /* Enable TIMER0 interrupt vector in NVIC */
	    // NVIC_EnableIRQ(TIMER1_IRQn);
	    //   
	    // /* Set TIMER Top value */
	    // TIMER_TopSet(TIMER1, TOP);
	    //   
	    // /* Configure TIMER */
	    // TIMER_Init(TIMER1, &timerInit);
    /* Initialize LETIMER */
    LETIMER_setup();
	
    /* Enable underflow interrupt */  
    LETIMER_IntEnable(LETIMER0, LETIMER_IF_UF);  
  
    /* Enable LETIMER0 interrupt vector in NVIC*/
    NVIC_EnableIRQ(LETIMER0_IRQn);
 
}

void LETIMER0_IRQHandler(void)
{ 
  /* Clear LETIMER0 underflow interrupt flag */
  LETIMER_IntClear(LETIMER0, LETIMER_IF_UF);

  /* If the value of comp1 is over 0, decrement it 
     or bring comp1 back to COMPMAX (500) */
  if(comp1 != 0) 
    comp1--;
  else
    comp1 = COMPMAX;
  
  /* Write the new compare value to COMP1 */
  LETIMER_CompareSet(LETIMER0, 1, comp1);
  
  #ifdef SENSE_TEMP
	uint8_t buf[192 *2];
	TC74RegReadN(ReadTemp,1,buf);
	send_packet(SELF, TEMP_SENSOR, (uint16_t)buf[0]);
  #endif
	
  #ifdef SENSE_LIGHT
	uint8_t buf[192 *2];
	MAXRegReadN(LUX_HI, 1, buf);
	send_packet(SELF, LIGHT_SENSOR, (uint16_t)buf[0]);
  #endif

  #ifdef SENSE_IR
	int detect = GPIO_PinInGet(3,10);
	send_packet(SELF, IR_SENSOR, (uint16_t)detect);
  #endif
	
	
}

void LETIMER_setup(void)
{
  /* Enable necessary clocks */
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
  CMU_ClockEnable(cmuClock_CORELE, true);
  CMU_ClockEnable(cmuClock_LETIMER0, true);  
  CMU_ClockEnable(cmuClock_GPIO, true);
  
  /* Configure PD6 and PD7 as push pull so the
     LETIMER can override them */
  GPIO_PinModeSet(gpioPortD, 6, gpioModePushPull, 0);
  GPIO_PinModeSet(gpioPortD, 7, gpioModePushPull, 0);
  
  /* Set initial compare values for COMP0 and COMP1 
     COMP1 keeps it's value and is used as TOP value
     for the LETIMER.
     COMP1 gets decremented through the program execution
     to generate a different PWM duty cycle */
  LETIMER_CompareSet(LETIMER0, 0, COMPMAX);
  LETIMER_CompareSet(LETIMER0, 1, COMPMAX);
  
  /* Repetition values must be nonzero so that the outputs
     return switch between idle and active state */
  LETIMER_RepeatSet(LETIMER0, 0, 0x01);
  LETIMER_RepeatSet(LETIMER0, 1, 0x01);
  
  /* Route LETIMER to location 0 (PD6 and PD7) and enable outputs */
  LETIMER0->ROUTE = LETIMER_ROUTE_OUT0PEN | LETIMER_ROUTE_OUT1PEN | LETIMER_ROUTE_LOCATION_LOC0;
  
  /* Set configurations for LETIMER 0 */
  const LETIMER_Init_TypeDef letimerInit = 
  {
  .enable         = true,                   /* Start counting when init completed. */
  .debugRun       = false,                  /* Counter shall not keep running during debug halt. */
  .rtcComp0Enable = false,                  /* Don't start counting on RTC COMP0 match. */
  .rtcComp1Enable = false,                  /* Don't start counting on RTC COMP1 match. */
  .comp0Top       = true,                   /* Load COMP0 register into CNT when counter underflows. COMP0 is used as TOP */
  .bufTop         = false,                  /* Don't load COMP1 into COMP0 when REP0 reaches 0. */
  .out0Pol        = 0,                      /* Idle value for output 0. */
  .out1Pol        = 0,                      /* Idle value for output 1. */
  .ufoa0          = letimerUFOAPwm,         /* PWM output on output 0 */
  .ufoa1          = letimerUFOAPulse,       /* Pulse output on output 1*/
  .repMode        = letimerRepeatFree       /* Count until stopped */
  };
  
  /* Initialize LETIMER */
  LETIMER_Init(LETIMER0, &letimerInit); 
}


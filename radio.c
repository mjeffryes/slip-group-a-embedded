#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "efm32.h"
#include "efm32_chip.h"
#include "efm32_emu.h"
#include "efm32_gpio.h"
#include "efm32_i2c.h"
#include "efm32_usart.h"
#include "efm32_rtc.h"
#include "efm32_cmu.h"
#include "efm32_adc.h"
#include "efm32_timer.h"
#include "efm32_int.h"
#include "config.h"
#include "nrf24.h"
#include "nrf24l01.h"
#include "nrf24_config.h"
#include <math.h>
#include "MMA845XQ.h"
#include "radio.h"
#include "router.h"


#define PACKET_TYPE_packet 1


//volatile uint8_t nrf_status = 0;
uint8_t buf[192*2];

uint8_t radio_buf[32];

volatile uint8_t nrf_status = 0;



typedef struct {
  
  uint8_t nodeID;
  uint8_t deviceID;
  int16_t data;
  uint8_t padding[28];
} Packet_Type __attribute__ ((packed));

Packet_Type packet;

void send_packet(uint8_t node, uint8_t device, uint16_t dat)
  {
  
  
	  packet.nodeID = node;
      packet.deviceID = device;
      packet.data = dat;

       // printf("sending nodeID: %d, device: %d, data: %d\n", packet.nodeID, packet.deviceID, packet.data);
      // NRF_PowerUp();
	 
      NRF_TransmitPacket(32, (uint8_t *)&packet);
	  GPIO->P[0].DOUT ^= (1 << device);
	  //NRF_WriteRegister(NRF_CONFIG, 0x0E); // Power Up, Transmitter
      //NRF_SendCommand(0xE1, 0xFF);
      //NRF_CE_hi;
	      //LEDOoff();
      RXEN_hi;
	  //NRF_PowerDown();
      //GPIO->P[RXEN_PORT].DOUT |= (1 << RXEN_PIN); 
  }
  
void receive_packet(int fildev)
	{
	RXEN_lo; 
      NRF_CE_lo;
      nrf_status = NRF_ReadRegister(NRF_STATUS);
      
      if (nrf_status & 0x10) 
      {
        NRF_WriteRegister(NRF_STATUS, 0x10);
      } else if (nrf_status & 0x20)
      {
        NRF_WriteRegister(NRF_STATUS, 0x20);
      } else if (nrf_status & 0x40)
      {
        NRF_WriteRegister(NRF_STATUS, 0x70);
   		
		NRF_ReceivePayload(NRF_R_RX_PAYLOAD, 32, (uint8_t *)&packet);
		
		// if(packet.deviceID != fildev){
		// printf("no match\n");
		// }
		// if(packet.deviceID == fildev){	
		// 	printf("match\n");
			GPIO->P[0].DOUT ^= (1 << packet.deviceID);
			sendPacket(packet.nodeID, packet.deviceID, packet.data);
        // }
		NRF_SendCommand(NRF_FLUSH_RX, 0xFF);
        RXEN_hi; 
        NRF_CE_hi;
      }
      
      INT_Disable();
      // nrfint();
      INT_Enable();
	}
  
 void setup_receiver(void)
 {
	NRF_EnableRX();
	RXEN_hi;
  }
void clearRadio(void)
{
  NRF_WriteRegister(NRF_STATUS, 0x70); // Clear all radio interrupts
}
#include <stdint.h>
#include <stdbool.h>

#import "remote.h"

void performAction(uint8_t action, uint16_t value) {
	usePlug(action, value);
}
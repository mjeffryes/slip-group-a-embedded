typedef enum
{
  PLUG_A,
  PLUG_B,
  PLUG_C
} PLUGS;

typedef enum
{
	BUTTON_OFF = 0,
  BUTTON_ON
} BUTTONS;

void usePlug(PLUGS plug, BUTTONS button);
#include <stdint.h>
#include <stdbool.h>
#include "efm32_chip.h"
#include "config.h"
#import "connect.h"
#import "actuate.h"
#import "sense.h"
#import "efm32_emu.h"
#import "radio.h"
#import "NRF24.h"
#import "nRF24L01.h"
#import "nrf24_config.h"
#define SELF 0

static volatile int NRF_Interrupt = 0;

void GPIO_EVEN_IRQHandler(void) 
{
  /* Acknowledge interrupt */
  if (GPIO->IF & (1 << NRF_INT_PIN)) 
  {
    NRF_Interrupt++;
    GPIO->IFC = (1 << NRF_INT_PIN);
  }
  receive_packet(1);
}


void handlePacket(uint64_t packet) {
	uint32_t pac = packet & 0xffffffff;
	uint8_t device = (pac & 0xff000000) >> 24;
	uint8_t action = (pac & 0x00ff0000) >> 16;
	uint8_t value = pac & 0x0000ffff;
	if (device == SELF) {
		performAction(action, value);
	}
	else {
		// send over network
	}
	
}

void sendPacket(uint8_t fromDevice, uint8_t action, uint16_t value) {
	uint32_t packet = ((uint32_t)fromDevice << 24 |  ((uint32_t)action) << 16 | value) & 0xffffffff;
	sendBits(packet, 24);
}

void main() {
  CHIP_Init();
  SystemCoreClockUpdate();
  IO_Init();
	uart_init(UART1);
	
	waitForConnect(&handlePacket);
	//sense();
	setupSensor();
  #ifndef BASESTATION 
  printf("sending\n");
//  NRF_PowerDown(); // power down radio until we need it to save power

  #endif // !BASESTATION

  NRF_SetupTX(); // set up radio

  #ifdef BASESTATION
  printf("receiving\n");
  setup_receiver();
  #endif // BASESTATION
  
  NRF_WriteRegister(NRF_STATUS, 0x70); // Clear all radio interrupts
	
    while(1)
                {
                  /* Go to EM1 */
                  EMU_EnterEM1();
                } 
			
}
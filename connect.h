typedef enum { DISCONNECTED, CONNECTED } ConnectionState;

typedef enum { LED_RED=0, LED_GREEN=1, LED_BLUE=3 } LEDPin;

typedef void (*PacketHandlerType)(uint64_t);

void waitForConnect(PacketHandlerType handler);

void LEDOn(LEDPin pin);

void LEDOff(LEDPin pin);

void LEDToggle(LEDPin pin);

void UART1_RX_IRQHandler(void);

void sendBits(uint64_t word, uint8_t length);

void handleBits(uint64_t bits);
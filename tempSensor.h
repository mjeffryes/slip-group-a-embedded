#define TC74_ADDR 0x9A
#define ReadTemp 0x00
#define Config 0x01

int TC74_RegisterGet(I2C_TypeDef *i2c,
                         uint8_t addr,
                         uint8_t reg,
                         uint8_t *buf,
						 uint8_t len);
						 
void TC74RegWrite(uint8_t reg, uint8_t val);
uint8_t TC74RegRead(uint8_t reg);
void TC74RegReadN(uint8_t reg, uint8_t len, uint8_t *buf);
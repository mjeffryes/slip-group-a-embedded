#include <stddef.h>
#include "efm32.h"
#include "NRF24.h"
#include "NRF24L01.h"
#include "efm32_usart.h"
#include "efm32_emu.h"
#include "efm32_i2c.h"
#include "config.h"
#include "MMA845XQ.h"
#include "tempSensor.h"

int TC74_RegisterGet(I2C_TypeDef *i2c,
                         uint8_t addr,
                         uint8_t reg,
                         uint8_t *buf,
						 uint8_t len)
{
  I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t regid[1];
  //uint8_t data[1];

  seq.addr = TC74_ADDR;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select register to be read */
  regid[0] = reg; //address of register not address of device
  seq.buf[0].data = regid;
  seq.buf[0].len = 1;
  seq.buf[1].data = buf; 
  seq.buf[1].len = len;
  ret = I2CDRV_Transfer(&seq);
  if (ret != i2cTransferDone)
  {
    return((int)ret);
  }

  return(0);
}

void TC74RegWrite(uint8_t reg, uint8_t val) {
   I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t regid[1];
  //uint8_t data[1];

  seq.addr = TC74_ADDR;
  seq.flags = I2C_FLAG_WRITE_WRITE;
  /* Select register to be read */
  regid[0] = reg;
  seq.buf[0].data = regid;
  seq.buf[0].len = 1;
  seq.buf[1].data = &val;
  seq.buf[1].len = 1;
 
  ret = I2CDRV_Transfer(&seq);
}

uint8_t TC74RegRead(uint8_t reg) {
  
  I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t regid[1];
  uint8_t data[1];

  seq.addr = TC74_ADDR;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select register to be read */
  regid[0] = reg;
  seq.buf[0].data = regid;
  seq.buf[0].len = 1;
  seq.buf[1].data = data;
  seq.buf[1].len = 1;
 
  ret = I2CDRV_Transfer(&seq);
  if (ret != i2cTransferDone)
  {
    return 0;
  }

  return data[0];
}

void TC74RegReadN(uint8_t reg, uint8_t len, uint8_t *buf)
{
	TC74_RegisterGet(I2C0, TC74_ADDR, reg, buf, len);
}

      
